const express = require('express');
const router = express.Router();
let currentAccounts = require('./current-accounts');
let termDeposits = require('./term-deposits');

router.get('/', (req, res) => {
  res.send('test api!');
});

router.get('/application/init', (req, res) => {
  res.status(200).json({ init: true });
});

router.get('/currentAccounts', (req, res) => res.status(200).json(currentAccounts));

router.get('/termDeposits', (req, res) => res.status(200).json(termDeposits));

router.get('/currentAccounts/:id', (req, res) => {
  const selectedAccount = currentAccounts.find(x => x.interest === Number(req.params.id));
  res.status(200).json(selectedAccount);
});

router.get('/termDeposits/:id', (req, res) => {
  const selectedDeposit = currentAccounts.find(x => x.interest === Number(req.params.id));
  res.status(200).json(selectedDeposit);
});

router.delete('/currentAccounts/:id', (req, res) => {
  currentAccounts = currentAccounts.filter(x => x.interest !== Number(req.params.id))
  res.status(200).json({removed: true});
})

router.delete('/termDeposits/:id', (req, res) => {
  termDeposits = termDeposits.filter(x => x.interest !== Number(req.params.id))
  res.status(200).json({removed: true});
})

module.exports = router;
