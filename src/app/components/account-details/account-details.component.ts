import { Component, OnInit } from '@angular/core';
import { AccountsService } from './../../providers/accounts/accounts.service';
import { ActivatedRoute } from '@angular/router';
import { Account } from './../../interfaces/account.interface';
import { Location } from '@angular/common';

@Component({
  selector: 'app-account-details',
  templateUrl: './account-details.component.html',
  styleUrls: ['./account-details.component.css']
})
export class AccountDetailsComponent implements OnInit {
  id: number;
  account: Account;

  constructor(
    private accountsService: AccountsService,
    private route: ActivatedRoute,
    private location: Location) {}

  ngOnInit() {
    this.route.params.subscribe(res => this.id = res.id);
    this.accountsService.getAccount(this.id).subscribe(account => this.account = account);
  }

  goBack(): void {
    this.location.back();
  }

}
