import { Component, OnInit } from '@angular/core';
import { DepositsService } from './../../providers/deposits/deposits.service';
import { ActivatedRoute } from '@angular/router';
import { Deposit } from './../../interfaces/deposit.interface';
import { Location } from '@angular/common';

@Component({
  selector: 'app-deposit-details',
  templateUrl: './deposit-details.component.html',
  styleUrls: ['./deposit-details.component.css']
})
export class DepositDetailsComponent implements OnInit {

  id: number;
  deposit: Deposit;

  constructor(
    private accountsService: DepositsService,
    private route: ActivatedRoute,
    private location: Location) { }

  ngOnInit() {
    this.route.params.subscribe(res => this.id = res.id);
    this.accountsService.getDeposit(this.id).subscribe(deposit => this.deposit = deposit);
  }

  goBack(): void {
    this.location.back();
  }

}
