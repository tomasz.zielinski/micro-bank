import { Component, OnInit } from '@angular/core';
import { AccountsService } from './../../providers/accounts/accounts.service';
import { MatTableDataSource, MatDialog } from '@angular/material';
import { Account } from './../../interfaces/account.interface';
import { ConfirmComponent } from './../common/confirm/confirm.component';


@Component({
  selector: 'app-accounts',
  templateUrl: './accounts.component.html',
  styleUrls: ['./accounts.component.css']
})
export class AccountsComponent implements OnInit {
  accounts: Account[];
  displayedColumns = ['interest', 'amount', 'account', 'actions'];
  dataSource: MatTableDataSource<Account>

  constructor(private accountsService: AccountsService, public dialog: MatDialog) { }

  ngOnInit() {
    this.updateAccounts();
  }

  updateAccounts(): void {
    this.accountsService.getCurrentAccounts().subscribe(accounts => {
      this.accounts = accounts;
      this.dataSource = new MatTableDataSource<Account>(this.accounts);
    });
  }

  openDialog(id: number): void {
    const dialogRef = this.dialog.open(ConfirmComponent, {
      data: { id }
    });

    dialogRef.afterClosed().subscribe(interest => {
      if (interest) {
        this.accountsService.removeAccount(interest).subscribe(resp => {
          if (resp.removed) {
            this.updateAccounts();
          }
        });
      }
    });
  }

}

