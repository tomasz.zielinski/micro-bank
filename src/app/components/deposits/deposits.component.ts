import { Component, OnInit } from '@angular/core';
import { DepositsService } from './../../providers/deposits/deposits.service';
import { MatTableDataSource, MatDialog } from '@angular/material';
import { Deposit } from './../../interfaces/deposit.interface';
import { ConfirmComponent } from './../common/confirm/confirm.component';

@Component({
  selector: 'app-deposits',
  templateUrl: './deposits.component.html',
  styleUrls: ['./deposits.component.css']
})
export class DepositsComponent implements OnInit {
  deposits: Deposit[];
  displayedColumns = ['interest', 'amount', 'account', 'actions'];
  dataSource: MatTableDataSource<Deposit>

  constructor(private depositsService: DepositsService, public dialog: MatDialog) { }

  ngOnInit() {
    this.updateDeposits();
  }

  updateDeposits(): void {
    this.depositsService.getCurrentDeposits().subscribe(deposits => {
      this.deposits = deposits;
      this.dataSource = new MatTableDataSource<Deposit>(this.deposits);
    });
  }

  openDialog(id: number): void {
    const dialogRef = this.dialog.open(ConfirmComponent, {
      data: { id }
    });

    dialogRef.afterClosed().subscribe(interest => {
      if (interest) {
        this.depositsService.removeDeposit(interest).subscribe(resp => {
          if (resp.removed) {
            this.updateDeposits();
          }
        });
      }
    });
  }

}
