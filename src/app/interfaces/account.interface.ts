export interface Account {
  account: string;
  amount: number;
  interest: number;
}