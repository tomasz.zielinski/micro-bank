export interface Deposit {
  account: string;
  amount: number;
  interest: number;
}