import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule, Routes } from '@angular/router';
import { MatButtonModule, MatTableModule, MatDialogModule, MatCardModule } from '@angular/material';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';

import { AccountsService } from './providers/accounts/accounts.service';
import { DepositsService } from './providers/deposits/deposits.service';
import { ApplicationService } from './providers/application/application.service';

import { AppComponent } from './app.component';
import { HomeComponent } from './components/home/home.component';
import { AccountsComponent } from './components/accounts/accounts.component';
import { DepositsComponent } from './components/deposits/deposits.component';
import { ConfirmComponent } from './components/common/confirm/confirm.component';
import { AccountDetailsComponent } from './components/account-details/account-details.component';
import { DepositDetailsComponent } from './components/deposit-details/deposit-details.component';

const ROUTES: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: 'home', component: HomeComponent },
  { path: 'accounts', component: AccountsComponent },
  { path: 'deposits', component: DepositsComponent },
  { path: 'account/:id', component: AccountDetailsComponent },
  { path: 'deposit/:id', component: DepositDetailsComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    AccountsComponent,
    DepositsComponent,
    ConfirmComponent,
    AccountDetailsComponent,
    DepositDetailsComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    NoopAnimationsModule,
    RouterModule.forRoot(ROUTES),
    // ng material
    MatButtonModule,
    MatTableModule,
    MatDialogModule,
    MatCardModule
  ],
  providers: [
    AccountsService,
    DepositsService,
    ApplicationService
  ],
  bootstrap: [AppComponent],
  entryComponents: [
    ConfirmComponent
  ]
})
export class AppModule { }
