import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import { Account } from './../../interfaces/account.interface';

@Injectable()
export class AccountsService {

  constructor(private http: Http) { }

  getCurrentAccounts(): Observable<Account[]> {
    return this.http.get('/api/currentAccounts')
      .map(res => res.json());
  }

  getAccount(id: number): Observable<Account> {
    return this.http.get(`/api/currentAccounts/${id}`)
      .map(res => res.json());
  }

  removeAccount(id: number) {
    return this.http.delete(`/api/currentAccounts/${id}`)
      .map(res => res.json());
  }

}