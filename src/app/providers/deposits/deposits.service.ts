import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import { Deposit } from './../../interfaces/deposit.interface';

@Injectable()
export class DepositsService {

  constructor(private http: Http) { }

  getCurrentDeposits(): Observable<Deposit[]> {
    return this.http.get('/api/termDeposits')
      .map(res => res.json());
  }

  getDeposit(id: number): Observable<Deposit> {
    return this.http.get(`/api/termDeposits/${id}`)
      .map(res => res.json());
  }

  removeDeposit(id: number) {
    return this.http.delete(`/api/termDeposits/${id}`)
      .map(res => res.json());
  }

}
