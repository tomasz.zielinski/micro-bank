import { TestBed, inject } from '@angular/core/testing';

import { DepositsService } from './deposits.service';

describe('DepositsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [DepositsService]
    });
  });

  it('should be created', inject([DepositsService], (service: DepositsService) => {
    expect(service).toBeTruthy();
  }));
});
