import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import { Status } from './../../interfaces/status.interface';

@Injectable()
export class ApplicationService {

  constructor(private http: Http) { }

  getStatus(): Observable<Status> {
    return this.http.get('api/application/init')
      .map(res => res.json());
  }

}
