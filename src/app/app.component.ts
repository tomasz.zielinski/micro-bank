import { Component, OnInit } from '@angular/core';
import { ApplicationService } from './providers/application/application.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  status: boolean;

  constructor(private applicationService: ApplicationService) {}

  ngOnInit() {
    this.checkStatus();
  }

  checkStatus() {
    this.applicationService.getStatus().subscribe(resp => {
      if (resp.init) {
        this.status = resp.init;
        return;
      }
      this.checkStatus();
    });
  }
}
